import { getCourses } from "./getCourses.mjs";
import fs from "fs";

(async () => {
  let results = await getCourses();

  let jsonString = JSON.stringify(unifyCourses(results));
  fs.writeFileSync("./JSON/courses.json", jsonString, "utf-8");
})();

// Helper function to unify courses
const unifyCourses = (jsonData) => {
  let finalSet = new Set();
  for (let i = 0; i < jsonData.coursesCodeSet.length; i++) {
    let newObj = {
      ID: `${i}`,
      CourseCode: `${jsonData.coursesCodeSet[i].CourseCode}`,
      CourseName: `${jsonData.coursesSet[i].CourseName}`,
    };
    finalSet.add(newObj);
  }
  return {
    courses: [...finalSet],
  };
};
