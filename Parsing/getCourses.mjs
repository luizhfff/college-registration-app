import cheerio from "cheerio";
import axios from "axios";
const url =
  "https://full-stack-web-development-ii.koehler.ca/langara-schedule-202010.html";

var coursesSet = new Set();
var coursesCodeSet = new Set();

const fetchData = async () => {
  const result = await axios.get(url);
  return cheerio.load(result.data);
};

const getCourses = async () => {
  const $ = await fetchData();

  $(".LargerText").each((index, element) => {
    let courseName = $(element).text();
    let courseObj = { ID: `${index}`, CourseName: `${courseName}` };
    coursesSet.add(courseObj);
  });

  $("tr > td > p > a").each((index, element) => {
    let courseCode = element.attribs.name;
    let courseCodeObj = { ID: `${index}`, CourseCode: `${courseCode}` };
    coursesCodeSet.add(courseCodeObj);
  });

  return {
    coursesSet: [...coursesSet],
    coursesCodeSet: [...coursesCodeSet],
  };
};

export { getCourses };
