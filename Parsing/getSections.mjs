import cheerio from "cheerio";
import axios from "axios";
const url =
  "https://full-stack-web-development-ii.koehler.ca/langara-schedule-202010.html";

var sectionsSet = new Set();
var onlineSectionsSet = new Set();

const fetchData = async () => {
  const result = await axios.get(url);
  return cheerio.load(result.data);
};

const getSections = async () => {
  const $ = await fetchData();

  $(".dataentrytable > tbody > tr").each((index, element) => {
    if (!$(element).attr("bgcolor") && $(element).find("em") == "") {
      let text = $(element).text();
      if (text.length > 20) {
        text = text.replace(/(\n|\r|\t)/gm, " ");
        let textArray = text.split(" ");
        textArray = textArray.filter((item) => item.length > 2);

        // Only getting the valid courses (not cancelled)
        if (
          !textArray.includes("**Cancelled**") &&
          textArray[0] != "Cancel" &&
          !textArray.includes("COOP") &&
          !textArray.includes("Co-operative") &&
          !textArray.includes("***NEW")
        ) {
          // Getting the online courses
          if (textArray.includes("component.") && textArray.includes("WWW")) {
            let sectionObject = {
              CRN: `${textArray[0]}`,
              Subj: `${textArray[1]}`,
              Crse: `${textArray[2]}`,
              Sec: `${textArray[3]}`,
              Cred: `${textArray[4]}`,
              Title: `${textArray[5]}`,
              Instructor: `${textArray[textArray.length - 1]}`,
            };
            onlineSectionsSet.add(sectionObject);
          }
          // Getting the presential courses
          else if (
            !textArray.includes("component.") &&
            !textArray.includes("WWW")
          ) {
            let sectionObject = {
              CRN: `${textArray[0]}`,
              Subj: `${textArray[1]}`,
              Crse: `${textArray[2]}`,
              Sec: `${textArray[3]}`,
              Cred: `${textArray[4]}`,
              Title: `${textArray[5]}` + " " + `${textArray[6]}`,
              Type: `${textArray[textArray.length - 5]}`,
              Days: `${textArray[textArray.length - 4]}`,
              Time: `${textArray[textArray.length - 3]}`,
              Room: `${textArray[textArray.length - 2]}`,
              Instructor: `${textArray[textArray.length - 1]}`,
            };
            sectionsSet.add(sectionObject);
          }
        }
      }
    }
  });
  return {
    onlineSectionsSet: [...onlineSectionsSet].sort(),
    sectionsSet: [...sectionsSet].sort(),
  };
};

export { getSections };
