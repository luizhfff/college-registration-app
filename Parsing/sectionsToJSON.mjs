import { getSections } from "./getSections.mjs";
import fs from "fs";

(async () => {
  let results = await getSections();
  let onlineCoursesJsonString = JSON.stringify(
    mergeClasses(results.onlineSectionsSet)
  );
  let coursesJsonString = JSON.stringify(mergeClasses(results.sectionsSet));
  fs.writeFileSync(
    "./JSON/onlineSections.json",
    onlineCoursesJsonString,
    "utf-8"
  );
  fs.writeFileSync(
    "./JSON/presentialSections.json",
    coursesJsonString,
    "utf-8"
  );
})();

// Helper function to merge classes into same section
const mergeClasses = (jsonObj) => {
  let positionsToDelete = [];
  for (let i = 0; i < jsonObj.length; i++) {
    let lastElement = jsonObj[i - 1];
    let element = jsonObj[i];

    if (element != null && element.CRN == "Lab") {
      lastElement.Days = Object.assign(
        { 0: lastElement.Days },
        { 1: element.Days }
      );
      lastElement.Time = Object.assign(
        { 0: lastElement.Time },
        { 1: element.Time }
      );
      lastElement.Room = Object.assign(
        { 0: lastElement.Room },
        { 1: element.Room }
      );
      lastElement.Type = Object.assign(
        { 0: lastElement.Type },
        { 1: element.Type }
      );

      positionsToDelete.push(i);
    }
  }

  for (let i = 0; i < positionsToDelete.length; i++) {
    delete jsonObj[positionsToDelete[i]];
  }

  positionsToDelete = [];

  for (let i = 0; i < jsonObj.length; i++) {
    let lastElement = jsonObj[i - 1];
    let element = jsonObj[i];

    if (element != null && element.CRN == "Lecture") {
      lastElement.Days = Object.assign(
        { 0: lastElement.Days },
        { 1: element.Days }
      );
      lastElement.Time = Object.assign(
        { 0: lastElement.Time },
        { 1: element.Time }
      );
      lastElement.Room = Object.assign(
        { 0: lastElement.Room },
        { 1: element.Room }
      );
      lastElement.Type = Object.assign(
        { 0: lastElement.Type },
        { 1: element.Type }
      );

      positionsToDelete.push(i);
    }
  }

  for (let i = 0; i < positionsToDelete.length; i++) {
    delete jsonObj[positionsToDelete[i]];
  }

  // filter all null objects
  return jsonObj.filter((element) => !null);
};
