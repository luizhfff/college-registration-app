#!/bin/bash

git clone git@gitlab.com:luizhfff/college-registration-app.git
cd college-registration-app/Parsing
npm install
node --experimental-json-modules --experimental-modules postCoursesToAPI.mjs
sleep 10s
node --experimental-json-modules --experimental-modules postSectionsToAPI.mjs