#!/bin/bash

# Docker-compose stop all containers
cd /home/luizfffca/college-registration-app
docker-compose down
cd

# Delete the old repo
sudo rm -rf /home/luizfffca/college-registration-app

# any future command that fails will exit the script
set -e

# Git cloning repo from GitLab
git clone git@gitlab.com:luizhfff/college-registration-app.git

sudo chmod -R 777 /home/luizfffca/college-registration-app

cd /home/luizfffca/college-registration-app

# Install node modules
#cd /home/luizfffca/cpsc2650-project/frontend
#sudo npm install
cd /home/luizfffca/college-registration-app/backend
sudo npm install
cd /home/luizfffca/college-registration-app/Parsing
sudo npm install

# Run docker-compose file on deamon mode
cd /home/luizfffca/college-registration-app
docker-compose build
docker-compose up -d
