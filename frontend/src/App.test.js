import React from "react";
import { render } from "@testing-library/react";
import Home from "./Home";

test("renders App", () => {
  const { getByText } = render(<Home />);
  const linkElement = getByText(/Hello/i);
  expect(linkElement).toBeInTheDocument();
});
