import React from "react";
import Navigation from "./components/Navigation";
import { Col, Container, Row } from "react-bootstrap";

const Home = () => (
  <>
    <Navigation />
    <Container>
      <Row>
        <Col className="block-example border border-info mt-5 mr-4 ml-4 mb-5">
          <section>
            <h2>Hello, </h2>
            <h4>
              this is the college registration app built for educational
              purposes.
            </h4>
            <h4>Please visit the Registration page</h4>
          </section>
        </Col>
      </Row>
    </Container>
  </>
);

export default Home;
