import * as React from "react";
import { Paper } from "@material-ui/core";
import { ViewState, EditingState } from "@devexpress/dx-react-scheduler";
import {
  Scheduler,
  WeekView,
  MonthView,
  Appointments,
  ViewSwitcher,
  Toolbar,
  EditRecurrenceMenu,
} from "@devexpress/dx-react-scheduler-material-ui";

// Schedule Component to show classes timeline
class Schedule extends React.PureComponent {
  constructor(props) {
    super(props);
    const appointments = props.data;
    this.state = {
      data: appointments,
    };

    this.commitChanges = this.commitChanges.bind(this);
  }

  commitChanges({ added, changed, deleted }) {
    this.setState((state) => {
      let { data } = state;
      if (added) {
        const startingAddedId =
          data.length > 0 ? data[data.length - 1].id + 1 : 0;
        data = [...data, { id: startingAddedId, ...added }];
      }
      if (changed) {
        data = data.map((appointment) =>
          changed[appointment.id]
            ? { ...appointment, ...changed[appointment.id] }
            : appointment
        );
      }
      if (deleted !== undefined) {
        data = data.filter((appointment) => appointment.id !== deleted);
      }
      return { data };
    });
  }

  render() {
    const { data } = this.state;

    return (
      <Paper>
        <Scheduler data={data}>
          <ViewState defaultCurrentDate="2020-06-04" />
          <EditingState onCommitChanges={this.commitChanges} />
          <WeekView startDay={1} endDay={30} startHour={8} endHour={23} />
          <MonthView
            startDay={1}
            endDay={30}
            startDayHour={8}
            endDayHour={23}
          />
          <Appointments />

          <Toolbar />
          <ViewSwitcher />

          <EditRecurrenceMenu />
        </Scheduler>
      </Paper>
    );
  }
}

export default Schedule;
