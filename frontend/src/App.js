import React, { Component } from "react";

import ReactGA from "react-ga";
import { createBrowserHistory } from "history";

import Registration from "./Registration";
import Home from "./Home";

import "./App.css";

import { Router, Switch, Route } from "react-router-dom";

import client from "./feathers";

import { loadReCaptcha } from "react-recaptcha-google";

// initialize ReactGA
const trackingId = "G-6VY7W34ELF"; // Replace with your Google Analytics tracking ID
ReactGA.initialize(trackingId);

// set up history
const history = createBrowserHistory();

// Initialize google analytics page view tracking
history.listen((location) => {
  ReactGA.pageview(location.pathname); // Record a pageview for the given page
});

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    loadReCaptcha();
    const coursesService = client.service("courses");
    const sectionsService = client.service("sections");

    coursesService
      .find({ query: { $limit: 100, $sort: { CourseName: 1 } } })
      .then((coursesData) => {
        const courses = coursesData.data;
        this.setState({
          coursesService,
          courses,
        });
      });

    sectionsService
      .find({ query: { $limit: 1000, $sort: { Title: 1 } } })
      .then((sectionData) => {
        const sections = sectionData.data;
        this.setState({
          sectionsService,
          sections,
        });
      });
  }

  render() {
    return (
      <Router history={history}>
        <Switch>
          <Route path="/registration">
            <Registration
              courses={this.state.courses}
              coursesService={this.state.coursesService}
              sections={this.state.sections}
              sectionsService={this.state.sectionsService}
            />
          </Route>

          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </Router>
    );
  }
}

export default App;
