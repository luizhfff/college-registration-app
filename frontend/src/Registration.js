import React, { Component } from "react";
import { Col, Container, Form, Row, Table } from "react-bootstrap";
import Navigation from "./components/Navigation";
import Scheduler from "./components/Scheduler";

/**
 * Helper functions
 * */
function mergeData(object) {
  if (typeof object === "string") {
    return object;
  }
  let result;
  if (Object.keys(object[1]).length === 3) {
    result = [object[0], object[1]];
  } else if (Object.keys(object[1]).length === 2) {
    result = [object[0], object[1][0], object[1][1]];
  } else if (Object.keys(object[1]).length === 1) {
    result = [object[0], object[1][0]];
  } else if (Object.keys(object[1]).length > 2) {
    result = [object[0], object[1]];
  }
  return result;
}

function mergeObj(obj) {
  if (typeof obj === "string") {
    return obj;
  }
  return obj[0];
}

function stringToTime(time) {
  let stringSplit = time.split("-");
  let startHour = stringSplit[0].slice(0, 2);
  let startMinutes = stringSplit[0].slice(2, 4);
  let endHour = stringSplit[1].slice(0, 2);
  let endMinutes = stringSplit[1].slice(2, 4);

  return [
    Number(startHour),
    Number(startMinutes),
    Number(endHour),
    Number(endMinutes),
  ];
}

function generateRrule(days) {
  let pattern = "";
  if (typeof days === "string") {
    if (days === "M-W----") {
      pattern = "MO,WE";
    } else if (days === "-T-R---") {
      pattern = "TU,TH";
    } else if (days === "MTWR---") {
      pattern = "MO,TU,WE,TH";
    } else if (days === "--W----") {
      pattern = "WE";
    } else if (days === "---R---") {
      pattern = "TH";
    } else if (days === "MTW----") {
      pattern = "MO,TU,WE";
    } else if (days === "M------") {
      pattern = "MO";
    } else if (days === "-T-----") {
      pattern = "TU";
    } else if (days === "----F--") {
      pattern = "FR";
    } else {
      pattern = "MO,WE";
    }

    return `FREQ=WEEKLY;BYDAY=${pattern};INTERVAL=1`;
  } else if (typeof days === "object") {
    if (days[0] === "M-W----") {
      pattern = "MO,WE";
    } else if (days[0] === "-T-R---") {
      pattern = "TU,TH";
    } else if (days[0] === "MTWR---") {
      pattern = "MO,TU,WE,TH";
    } else if (days[0] === "--W----") {
      pattern = "WE";
    } else if (days[0] === "---R---") {
      pattern = "TH";
    } else if (days[0] === "MTW----") {
      pattern = "MO,TU,WE";
    } else if (days[0] === "M------") {
      pattern = "MO";
    } else if (days[0] === "-T-----") {
      pattern = "TU";
    } else if (days[0] === "----F--") {
      pattern = "FR";
    } else {
      pattern = "MO,WE";
    }
    return `FREQ=WEEKLY;BYDAY=${pattern};INTERVAL=1`;
  }

  return `FREQ=WEEKLY;BYDAY=MO,WE;INTERVAL=1`;
}

function formatSections(listOfCourses) {
  let formatedList = [];
  for (let i = 0; i < listOfCourses.length; i++) {
    let courseList = listOfCourses[i];
    for (let y = 0; y < courseList.length; y++) {
      let course = courseList[y];
      let time = stringToTime(mergeObj(course.Time));
      let title = `CRN : ${course.CRN} | Subj: ${course.Subj} | Course: ${course.Crse}`;
      let startDate = new Date(2020, 5, 1, time[0], time[1], 0);
      let endDate = new Date(2020, 5, 1, time[2], time[3], 0);
      let id = y;
      let rRule = generateRrule(mergeData(course.Days));
      formatedList.push({
        title: title,
        startDate: startDate,
        endDate: endDate,
        id: id,
        rRule: rRule,
      });
    }
  }
  return formatedList;
}

// Registration Component
class Registration extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.handleCourseChange = this.handleCourseChange.bind(this);
    this.handleSectionChange = this.handleSectionChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleCourseChange(event) {
    this.setState({ courseSelected: event.target.value });
  }

  handleSectionChange(event) {
    this.setState({ sectionSelected: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();
  }

  addCourseSection(course, event) {
    let allCourses = [];
    if (this.state.allCoursesSelected !== undefined) {
      allCourses = this.state.allCoursesSelected;
      allCourses.push([course[0], course[1]]);
    } else {
      allCourses = [[course[0], course[1]]];
    }

    this.setState({ allCoursesSelected: allCourses });
  }

  removeCourseSelected(course, event) {
    let arr = this.state.allCoursesSelected;
    for (let i = 0; i < arr.length; i++) {
      if (arr[i][0] === course[0] && arr[i][1] === course[1]) {
        arr[i][0] = "";
        arr[i][1] = "";
      }
    }
    let finalArr = arr.filter((crse, index, arr) => {
      return crse[0] !== "";
    });

    this.setState({ allCoursesSelected: finalArr });
  }

  processCourses(props) {
    const { sections } = props;
    let allSections = [];

    if (this.state.allCoursesSelected) {
      for (let i = 0; i < this.state.allCoursesSelected.length; i++) {
        let course = this.state.allCoursesSelected[i];
        let tempCourse = sections.filter((item, index, arr) => {
          if (item.Subj === course[0] && item.Crse === course[1]) {
            return true;
          }
          return false;
        });
        if (tempCourse) {
          allSections.push(tempCourse);
        }
      }

      let formattedSections = formatSections(allSections);

      this.setState({
        listOfSectionsProcessed: allSections,
        listOfSectionsFormatted: formattedSections,
      });
    }
  }

  render() {
    const { courses, sections } = this.props;
    let sectionsDisplayDataBlock;

    if (this.state.courseSelected) {
      const filteredSections = sections.filter((sectionElement, index, arr) => {
        return sectionElement.Subj === this.state.courseSelected;
      });

      const uniqueSections = [];
      const map = new Map();
      for (const item of filteredSections) {
        if (!map.has(item.Crse)) {
          map.set(item.Crse, true); // set any value to Map
          uniqueSections.push({
            _id: item._id,
            Subj: item.Subj,
            Crse: item.Crse,
            Cred: item.Cred,
            Title: item.Title,
            Instructor: item.Instructor,
          });
        }
      }

      sectionsDisplayDataBlock = (
        <Table striped bordered hover responsive>
          <thead>
            <tr>
              <th scope="col">Subj</th>
              <th scope="col">Course</th>
              <th scope="col">Credits</th>
              <th scope="col">Title</th>
              <th scope="col">Instructor</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            {uniqueSections &&
              uniqueSections.map((section) => (
                <tr key={section._id}>
                  <th scope="row"> {section.Subj} </th>
                  <td> {section.Crse} </td>
                  <td> {section.Cred} </td>
                  <td> {section.Title} </td>
                  <td> {section.Instructor} </td>
                  <td>
                    <button
                      onClick={this.addCourseSection.bind(this, [
                        section.Subj.toString(),
                        section.Crse.toString(),
                      ])}
                      type="button"
                      className="btn btn-primary"
                    >
                      Add to Schedule
                    </button>
                  </td>
                </tr>
              ))}
          </tbody>
        </Table>
      );
    }

    return (
      <>
        <Navigation />
        <Container fluid>
          <Row>
            <Col className="block-example border border-info mt-5 mr-4 ml-4 mb-5">
              <h2>Registration</h2>
              <section>
                <Form onSubmit={this.handleSubmit}>
                  <Form.Group controlId="department">
                    <Form.Label>Department</Form.Label>
                    <Form.Control
                      as="select"
                      onChange={this.handleCourseChange}
                      value={this.state.courseSelected}
                    >
                      <option> Select a department </option>
                      {courses &&
                        courses.map((course) => (
                          <option key={course.ID} value={course.CourseCode}>
                            {" "}
                            {course.CourseCode} | {course.CourseName}
                          </option>
                        ))}
                    </Form.Control>
                  </Form.Group>

                  <Form.Group controlId="section">
                    <Form.Label>Course</Form.Label>
                    {sectionsDisplayDataBlock}
                  </Form.Group>
                </Form>
              </section>
            </Col>

            <Col className="block-example border border-info mt-5 mr-4 ml-4 mb-5">
              <section>
                <h2>Courses Selected</h2>
                <Table>
                  {this.state.allCoursesSelected &&
                    this.state.allCoursesSelected.map((el) => (
                      <tr key={el[0]}>
                        <td> {el[0]} </td>
                        <td> {el[1]} </td>
                        <td>
                          <button
                            onClick={this.removeCourseSelected.bind(this, [
                              el[0].toString(),
                              el[1].toString(),
                            ])}
                            type="button"
                            className="btn btn-danger btn-sm"
                          >
                            Remove
                          </button>
                        </td>
                      </tr>
                    ))}
                </Table>
                <button
                  onClick={this.processCourses.bind(this, this.props)}
                  type="button"
                  className="btn btn-primary"
                >
                  Show Available Sections
                </button>
              </section>
              <section className="mt-5">
                <h2>List of Available Sections For All Courses Selected</h2>
                <Table striped bordered hover responsive>
                  <thead>
                    <tr>
                      <th scope="col">CRN</th>
                      <th scope="col">Subj</th>
                      <th scope="col">Course</th>
                      <th scope="col">Section</th>
                      <th scope="col">Credits</th>
                      <th scope="col">Title</th>
                      <th scope="col">Type</th>
                      <th scope="col">Days</th>
                      <th scope="col">Time</th>
                      <th scope="col">Room</th>
                      <th scope="col">Instructor</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.listOfSectionsProcessed &&
                      this.state.listOfSectionsProcessed.map((arrItem) =>
                        arrItem.map((section) => (
                          <tr key={section._id}>
                            <th scope="row">{section.CRN}</th>
                            <th scope="row"> {section.Subj} </th>
                            <td> {section.Crse} </td>
                            <td> {section.Sec} </td>
                            <td> {section.Cred} </td>
                            <td> {section.Title} </td>
                            <td>
                              {" "}
                              {typeof section.Type === "string"
                                ? section.Type
                                : mergeData(section.Type).map((type) => (
                                    <tr key={type}>
                                      <td>{type}</td>
                                    </tr>
                                  ))}{" "}
                            </td>
                            <td>
                              {" "}
                              {typeof section.Days === "string"
                                ? section.Days
                                : mergeData(section.Days).map((Days) => (
                                    <tr key={Days}>
                                      <td>{Days}</td>
                                    </tr>
                                  ))}{" "}
                            </td>
                            <td>
                              {" "}
                              {typeof section.Time === "string"
                                ? section.Time
                                : mergeData(section.Time).map((Time) => (
                                    <tr key={Time}>
                                      <td>{Time}</td>
                                    </tr>
                                  ))}{" "}
                            </td>
                            <td>
                              {" "}
                              {typeof section.Room === "string"
                                ? section.Room
                                : mergeData(section.Room).map((Room) => (
                                    <tr key={Room}>
                                      <td>{Room}</td>
                                    </tr>
                                  ))}{" "}
                            </td>
                            <td> {section.Instructor} </td>
                          </tr>
                        ))
                      )}
                  </tbody>
                </Table>
              </section>
            </Col>
          </Row>
          <Row className="block-example border border-info mx-4 my-4">
            <section>
              <h2>Term Schedule</h2>
              {this.state.listOfSectionsFormatted && (
                <Scheduler data={this.state.listOfSectionsFormatted} />
              )}
            </section>
          </Row>
        </Container>
      </>
    );
  }
}

export default Registration;
