import { Hook, HookContext } from "@feathersjs/feathers";

export default (options = {}): Hook => {
  return async (context: HookContext) => {
    const { params } = context;

    // @ts-ignore
    // Uses safeheader to verify if user is authorized to make changes
    if (params.headers.safeheader != "expotis@@!!") {
      throw new Error("User not authorized to make API calls !");
    }

    return context;
  };
};
