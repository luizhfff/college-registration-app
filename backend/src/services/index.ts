import { Application } from "../declarations";
import users from "./users/users.service";
import courses from "./courses/courses.service";
import sections from "./sections/sections.service";
// Don't remove this comment. It's needed to format import lines nicely.

export default function (app: Application) {
  app.configure(users);
  app.configure(courses);
  app.configure(sections);
}
