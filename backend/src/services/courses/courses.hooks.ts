import checkCourses from "../../hooks/check-courses";
export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [checkCourses()],
    update: [checkCourses()],
    patch: [checkCourses()],
    remove: [checkCourses()],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
