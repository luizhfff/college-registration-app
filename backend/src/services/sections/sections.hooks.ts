import checkSections from "../../hooks/check-sections";
export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [checkSections()],
    update: [checkSections()],
    patch: [checkSections()],
    remove: [checkSections()],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
