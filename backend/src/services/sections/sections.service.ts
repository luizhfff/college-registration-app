// Initializes the `sections` service on path `/sections`
import { ServiceAddons } from "@feathersjs/feathers";
import { Application } from "../../declarations";
import { Sections } from "./sections.class";
import hooks from "./sections.hooks";

// Add this service to the service type index
declare module "../../declarations" {
  interface ServiceTypes {
    sections: Sections & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const options = {
    paginate: app.get("paginate"),
  };

  // Initialize our service with any options it requires
  app.use("/sections", new Sections(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service("sections");

  service.hooks(hooks);
}
