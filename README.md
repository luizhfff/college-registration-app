# College Registration WebApp

#### Description : 
    * This project main purpose is to create an application that work as a registration application for a college. It
     utilizes these concepts: 
        * Containers (microservices)
        * CI/CD (automated integration and delivery)
        * DNS services
        * TLS
        * Restful API
        * Captcha
        * Parsing
        * Analytics
        * Accessibility
        * Static WebSite Hosting
 
####  Frontend : 
    * React JS

####  Backend : 
    * Feathers JS (with MongoDB adapter)
    * MongoDB

####  Parsing : 
    * A Node.JS module utilizing the modules Cheerio and Axios to do Web Scraping and turn a HTML page into JSON
     files to be later stored in MongoDB collections through FeathersJS REST HTTP API.

####  Implementation :
    * All backend services were implemented using Docker containers that are created through docker-compose in a
     single YAML file.
        * Services : backend | mongodb | nginx
        * Hosting machine for the Docker containers : Google Cloud Platform
     
    * Folder and Project Structure:
        * Backend : stores all FeathersJS package files
        * Confs : 
            * backend : stores Dockerfile configuration for the backend Docker container
            * deploy : deploying scripts to be used by GitLab CI/CD
            * nginx : stores the configuration files to be used by the NGINX server
        * Frontend : stores all React App package files to be later built and deployed to Firebase.
        * Parsing : stores parsing module (cheerio and axios)

####  Hosting : 
    * React frontend website is hosted statically in Google Firebase Hosting Services.

####  CI / CD :
    * GitLab CI/CD was chosen as Continuous Integration and Continuous Delivery implementation for this project. For
     that, there is a YAML file '.gitlab-ci.yml' describing the configuration for four stages:
        * Test : tests both frontend and backend code
        * Build : build frontend React code and deploy to Firebase
        * Deploy : execute bash script 'deploy.sh', which pulls the new code to the hosting machine after a Git Push.
        * Populate-DB : after all services are up and running, executes two modules inside Parsing module to post the
         courses and sections to FeathersJS REST HTTP API.

####  Analytics
    * Implemented Google Analytics

####  reCAPTCHA
    * Implemented Google reCAPTCHA